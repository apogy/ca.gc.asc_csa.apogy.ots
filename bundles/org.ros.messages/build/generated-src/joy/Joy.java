package joy;

public interface Joy extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "joy/Joy";
  static final java.lang.String _DEFINITION = "# This message is deprecated please use the sensor_msgs/Joy\nfloat32[] axes\nint32[] buttons\n";
  float[] getAxes();
  void setAxes(float[] value);
  int[] getButtons();
  void setButtons(int[] value);
}
