package wiimote;

public interface RumbleControl extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "wiimote/RumbleControl";
  static final java.lang.String _DEFINITION = "# Message to control the Wiimote rumble (vibrator).\n# We simply use a TimedSwitch. So rumble can\n# be turned on (switch_mode == 1), off (switch_mode == 0),\n# or pulsed with a timing pattern (switch_mode == -1).\n# Number of times the cycle is repeated: num_cycles\n# (-1: repeat till next RumbleControl message). pulse_pattern\n# contains the time on/off pattern. (see also TimedSwitch.msg)\n\nTimedSwitch rumble\n\n";
  wiimote.TimedSwitch getRumble();
  void setRumble(wiimote.TimedSwitch value);
}
