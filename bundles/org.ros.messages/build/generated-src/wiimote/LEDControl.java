package wiimote;

public interface LEDControl extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "wiimote/LEDControl";
  static final java.lang.String _DEFINITION = "# Message to request Wiimote LED operations. The Wiimote has four\n# LEDs. This message enables its user to turn each individual LED\n# on or off, and to define a blink pattern for each LEDs.\n#        See TimedSwitch.msg for details.\n\nTimedSwitch[] timed_switch_array  \n";
  java.util.List<wiimote.TimedSwitch> getTimedSwitchArray();
  void setTimedSwitchArray(java.util.List<wiimote.TimedSwitch> value);
}
