package clearpath_base;

public interface VelocitySetpt extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/VelocitySetpt";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 trans\nfloat64 rot\nfloat64 accel\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getTrans();
  void setTrans(double value);
  double getRot();
  void setRot(double value);
  double getAccel();
  void setAccel(double value);
}
