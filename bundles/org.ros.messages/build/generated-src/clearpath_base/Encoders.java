package clearpath_base;

public interface Encoders extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Encoders";
  static final java.lang.String _DEFINITION = "Header header\nEncoder[] encoders\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  java.util.List<clearpath_base.Encoder> getEncoders();
  void setEncoders(java.util.List<clearpath_base.Encoder> value);
}
