package clearpath_base;

public interface SafetyStatus extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/SafetyStatus";
  static final java.lang.String _DEFINITION = "Header header\nuint16 flags\nbool estop";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  short getFlags();
  void setFlags(short value);
  boolean getEstop();
  void setEstop(boolean value);
}
