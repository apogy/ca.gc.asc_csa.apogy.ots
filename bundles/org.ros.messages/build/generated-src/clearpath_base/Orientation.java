package clearpath_base;

public interface Orientation extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Orientation";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 roll\nfloat64 pitch\nfloat64 yaw";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getRoll();
  void setRoll(double value);
  double getPitch();
  void setPitch(double value);
  double getYaw();
  void setYaw(double value);
}
