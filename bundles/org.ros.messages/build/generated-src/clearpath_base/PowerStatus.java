package clearpath_base;

public interface PowerStatus extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/PowerStatus";
  static final java.lang.String _DEFINITION = "Header header\nPowerSource[] sources\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  java.util.List<clearpath_base.PowerSource> getSources();
  void setSources(java.util.List<clearpath_base.PowerSource> value);
}
