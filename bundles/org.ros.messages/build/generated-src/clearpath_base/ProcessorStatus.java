package clearpath_base;

public interface ProcessorStatus extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/ProcessorStatus";
  static final java.lang.String _DEFINITION = "Header header\nint32[] errors\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  int[] getErrors();
  void setErrors(int[] value);
}
