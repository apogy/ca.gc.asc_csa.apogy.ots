package clearpath_base;

public interface FirmwareInfo extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/FirmwareInfo";
  static final java.lang.String _DEFINITION = "Header header\nint8 firmware_major\nint8 firmware_minor\nint8 protocol_major\nint8 protocol_minor\nuint32 firmware_write_time\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  byte getFirmwareMajor();
  void setFirmwareMajor(byte value);
  byte getFirmwareMinor();
  void setFirmwareMinor(byte value);
  byte getProtocolMajor();
  void setProtocolMajor(byte value);
  byte getProtocolMinor();
  void setProtocolMinor(byte value);
  int getFirmwareWriteTime();
  void setFirmwareWriteTime(int value);
}
