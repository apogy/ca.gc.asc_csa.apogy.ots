package clearpath_base;

public interface TurnSetpt extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/TurnSetpt";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 trans_vel\nfloat64 turn_radius\nfloat64 trans_accel\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getTransVel();
  void setTransVel(double value);
  double getTurnRadius();
  void setTurnRadius(double value);
  double getTransAccel();
  void setTransAccel(double value);
}
