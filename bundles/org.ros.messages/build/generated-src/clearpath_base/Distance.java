package clearpath_base;

public interface Distance extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Distance";
  static final java.lang.String _DEFINITION = "Header header\nfloat64[] distances";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double[] getDistances();
  void setDistances(double[] value);
}
