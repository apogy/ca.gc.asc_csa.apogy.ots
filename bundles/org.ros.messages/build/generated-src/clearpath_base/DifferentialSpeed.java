package clearpath_base;

public interface DifferentialSpeed extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/DifferentialSpeed";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 left_speed\nfloat64 right_speed\nfloat64 left_accel\nfloat64 right_accel\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getLeftSpeed();
  void setLeftSpeed(double value);
  double getRightSpeed();
  void setRightSpeed(double value);
  double getLeftAccel();
  void setLeftAccel(double value);
  double getRightAccel();
  void setRightAccel(double value);
}
