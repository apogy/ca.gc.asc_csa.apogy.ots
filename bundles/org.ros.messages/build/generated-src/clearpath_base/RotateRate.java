package clearpath_base;

public interface RotateRate extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/RotateRate";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 roll\nfloat64 pitch\nfloat64 yaw\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getRoll();
  void setRoll(double value);
  double getPitch();
  void setPitch(double value);
  double getYaw();
  void setYaw(double value);
}
