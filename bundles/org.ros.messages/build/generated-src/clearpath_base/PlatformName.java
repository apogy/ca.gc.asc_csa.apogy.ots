package clearpath_base;

public interface PlatformName extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/PlatformName";
  static final java.lang.String _DEFINITION = "Header header\nstring name\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  java.lang.String getName();
  void setName(java.lang.String value);
}
