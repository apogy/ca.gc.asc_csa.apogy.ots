package clearpath_base;

public interface StateChange extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/StateChange";
  static final java.lang.String _DEFINITION = "string new_state\nstring joystick\n";
  java.lang.String getNewState();
  void setNewState(java.lang.String value);
  java.lang.String getJoystick();
  void setJoystick(java.lang.String value);
}
