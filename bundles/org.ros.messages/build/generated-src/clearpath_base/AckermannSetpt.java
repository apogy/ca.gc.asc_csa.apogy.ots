package clearpath_base;

public interface AckermannSetpt extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/AckermannSetpt";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 steering\nfloat64 throttle\nfloat64 brake\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getSteering();
  void setSteering(double value);
  double getThrottle();
  void setThrottle(double value);
  double getBrake();
  void setBrake(double value);
}
