package clearpath_base;

public interface Encoder extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/Encoder";
  static final java.lang.String _DEFINITION = "float64 travel\nfloat64 speed\n";
  double getTravel();
  void setTravel(double value);
  double getSpeed();
  void setSpeed(double value);
}
