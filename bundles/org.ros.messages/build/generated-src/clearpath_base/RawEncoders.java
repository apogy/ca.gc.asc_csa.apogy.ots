package clearpath_base;

public interface RawEncoders extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/RawEncoders";
  static final java.lang.String _DEFINITION = "Header header\nint32[] ticks\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  int[] getTicks();
  void setTicks(int[] value);
}
