package clearpath_base;

public interface DifferentialControl extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "clearpath_base/DifferentialControl";
  static final java.lang.String _DEFINITION = "Header header\nfloat64 l_p\nfloat64 l_i\nfloat64 l_d\nfloat64 l_ffwd\nfloat64 l_stic\nfloat64 l_sat\nfloat64 r_p\nfloat64 r_i\nfloat64 r_d\nfloat64 r_ffwd\nfloat64 r_stic\nfloat64 r_sat\n";
  std_msgs.Header getHeader();
  void setHeader(std_msgs.Header value);
  double getLP();
  void setLP(double value);
  double getLI();
  void setLI(double value);
  double getLD();
  void setLD(double value);
  double getLFfwd();
  void setLFfwd(double value);
  double getLStic();
  void setLStic(double value);
  double getLSat();
  void setLSat(double value);
  double getRP();
  void setRP(double value);
  double getRI();
  void setRI(double value);
  double getRD();
  void setRD(double value);
  double getRFfwd();
  void setRFfwd(double value);
  double getRStic();
  void setRStic(double value);
  double getRSat();
  void setRSat(double value);
}
