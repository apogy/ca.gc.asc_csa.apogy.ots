package test_rostopic;

public interface Floats extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rostopic/Floats";
  static final java.lang.String _DEFINITION = "float32 float32\nfloat64 float64\n";
  float getFloat32();
  void setFloat32(float value);
  double getFloat64();
  void setFloat64(double value);
}
