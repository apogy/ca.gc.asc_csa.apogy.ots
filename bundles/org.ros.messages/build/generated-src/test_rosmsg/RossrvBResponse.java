package test_rosmsg;

public interface RossrvBResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RossrvBResponse";
  static final java.lang.String _DEFINITION = "std_msgs/Empty empty";
  std_msgs.Empty getEmpty();
  void setEmpty(std_msgs.Empty value);
}
