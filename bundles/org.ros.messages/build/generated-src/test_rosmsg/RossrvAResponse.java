package test_rosmsg;

public interface RossrvAResponse extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RossrvAResponse";
  static final java.lang.String _DEFINITION = "int32 aresp";
  int getAresp();
  void setAresp(int value);
}
