package test_rosmsg;

public interface RossrvBRequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RossrvBRequest";
  static final java.lang.String _DEFINITION = "std_msgs/Empty empty\n";
  std_msgs.Empty getEmpty();
  void setEmpty(std_msgs.Empty value);
}
