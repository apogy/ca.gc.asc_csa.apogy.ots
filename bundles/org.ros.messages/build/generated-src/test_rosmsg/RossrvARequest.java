package test_rosmsg;

public interface RossrvARequest extends org.ros.internal.message.Message {
  static final java.lang.String _TYPE = "test_rosmsg/RossrvARequest";
  static final java.lang.String _DEFINITION = "int32 areq\n";
  int getAreq();
  void setAreq(int value);
}
