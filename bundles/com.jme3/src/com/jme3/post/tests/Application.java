package com.jme3.post.tests;

import org.eclipse.equinox.app.IApplication;
import org.eclipse.equinox.app.IApplicationContext;

import com.jme3.app.SimpleApplication;
import com.jme3.material.Material;
import com.jme3.math.ColorRGBA;
import com.jme3.scene.Geometry;
import com.jme3.scene.shape.Box;


public class Application implements IApplication 
{
	SimpleApplication app = null;
	
	@Override
	public Object start(IApplicationContext context) throws Exception 
	{
		getSimpleApplication().start();        
		return null;
	}

	@Override
	public void stop() 
	{		
		getSimpleApplication().stop();
	}
	
	protected SimpleApplication getSimpleApplication()
	{
		if(app == null)
		{
			app = new MyApp();
		}
		
		return app;
	}
	
	class MyApp extends SimpleApplication
	{
		protected Geometry player;
		
		@Override
		public void simpleInitApp() 
		{
			Box b = new Box(1, 1, 1); // create cube shape
	        Geometry geom = new Geometry("Box", b);  // create cube geometry from the shape
		    Material mat = new Material(assetManager,"Common/MatDefs/Misc/Unshaded.j3md");  // create a simple material
		    mat.setColor("Color", ColorRGBA.Blue);   // set color of material to blue
		    geom.setMaterial(mat);                   // set the cube's material
		    rootNode.attachChild(geom);              // make the cube appear in the scene
		    
		    Box bb = new Box(1, 1, 1);
		    player = new Geometry("blue cube", bb);
		    Material matPlayer = new Material(assetManager,        "Common/MatDefs/Misc/Unshaded.j3md");
		    
		    mat.setColor("Color", ColorRGBA.Blue);
		          player.setMaterial(matPlayer);
		          rootNode.attachChild(player);
		}
		
		 /* Use the main event loop to trigger repeating actions. */
	    @Override
	    public void simpleUpdate(float tpf) 
	    {
	        // make the player rotate:
	        player.rotate(0, 2*tpf, 0); 
	    }
	}
}
