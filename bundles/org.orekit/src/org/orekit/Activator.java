package org.orekit;

import java.io.File;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.net.URL;
import java.nio.file.Files;
import java.util.Enumeration;

import org.eclipse.core.runtime.IPath;
import org.eclipse.core.runtime.Path;
import org.eclipse.osgi.framework.log.FrameworkLog;
import org.eclipse.osgi.framework.log.FrameworkLogEntry;
import org.orekit.data.DataProvidersManager;
import org.orekit.data.DirectoryCrawler;
import org.orekit.time.TimeScalesFactory;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.ServiceReference;

public class Activator implements BundleActivator 
{
	public static String ID = "org.orekit";
	
	// Folder where data is located
	public static final String DATA_FOLDER = "data";
	
	// The shared instance
	private static Activator plugin;
	
	private static BundleContext context;
	
	public static BundleContext getContext() {
		return context;
	}

	private java.nio.file.Path tmpPath;

	public Activator() {
		plugin = this;
	}
	
	/**
	 * Returns the shared instance
	 * 
	 * @return the shared instance
	 */
	public static Activator getDefault() {
		return plugin;
	}
	
	@Override
	public void start(BundleContext context) throws Exception 
	{		
		Activator.context = context;
		
		try
		{	
			logMessage(this, "Loading OreKit data...", FrameworkLogEntry.INFO);
			
			// Copies the DATA_FOLDER to a temporary folder.
			tmpPath = copyDataFolderToTemp(DATA_FOLDER);
			
			// Loads OreKit configuration data
			DataProvidersManager.getInstance().addProvider(new DirectoryCrawler(tmpPath.toFile()));
			
			// Force a first load by calling a TimeScalesFactory.getUTC();
			try
			{
				TimeScalesFactory.getUTC();
			}
			catch(Throwable t)
			{				
			}
			
			logMessage(this, "Sucessfully loaded OreKit data.", FrameworkLogEntry.OK);
		}
		catch(Throwable t)
		{
			logMessage(this, "Error cocured when loading OreKit Data !", FrameworkLogEntry.ERROR, t);
		}
	}

	@Override
	public void stop(BundleContext context) throws Exception 
	{		
		deleteFile(tmpPath.toFile());
		
		Activator.context = null;
	}
	
	public static void deleteFile(File element) {
	    if (element.isDirectory()) {
	        for (File sub : element.listFiles()) {
	            deleteFile(sub);
	        }
	    }
	    element.delete();
	}	
	
	/**
	 * Return the FrameworkLog that is used to log messages within Symphony.
	 * @return The FrameworkLog used to log messages.
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static FrameworkLog getLog()
	{
		if(getContext() != null)
		{
			ServiceReference logService = getContext().getServiceReference("org.eclipse.osgi.framework.log.FrameworkLog");				
			FrameworkLog log = (FrameworkLog) getContext().getService(logService);		
			return log;
		}
		else
		{
			return null;
		}
	}

	/**
	 * Logs a message to the framework log.
	 * @param context The bundle context of the bundle containing the object generating the message.
	 * @param objectClass The object generating the message.
	 * @param message The message.
	 * @param severity The message severity : FrameworkLogEntry.OK, FrameworkLogEntry.INFO, FrameworkLogEntry.WARNING or FrameworkLogEntry.ERROR. 
	 * @param throwable Throwable associated with the message, can be null.
	 */
	public static void logMessage(Object object, String message, int severity, Throwable throwable)
	{
		String fullMessage = object.getClass().getSimpleName() + " : " + message;
		FrameworkLogEntry logEntry = new FrameworkLogEntry(getContext().getBundle().getSymbolicName(), severity, 0, fullMessage, 0, throwable,null);
		if(getLog() != null) getLog().log(logEntry);
	}
	
	/**
	 * Logs a message to the framework log.
	 * @param context The bundle context of the bundle containing the object generating the message.
	 * @param object The object generating the message.
	 * @param message The message.
	 * @param severity The message severity : FrameworkLogEntry.OK, FrameworkLogEntry.INFO, FrameworkLogEntry.WARNING or FrameworkLogEntry.ERROR.
	 */
	public static void logMessage(Object object, String message, int severity)
	{
		logMessage(object, message, severity, null);
	}	
	
	private java.nio.file.Path copyDataFolderToTemp(String folderPath) throws Exception
	{
		java.nio.file.Path tempPath = Files.createTempDirectory("oreKit");
		Enumeration<String> entries = getContext().getBundle().getEntryPaths(folderPath);
		
		while(entries.hasMoreElements())
		{
			try
			{
				String entry = entries.nextElement();
				URL url = getContext().getBundle().getEntry(entry);									
				IPath p = new Path(ID + File.separator + folderPath + File.separator +  url.getFile());		
				
				if(!p.hasTrailingSeparator())
				{							
					String fileName = entry.substring(entry.lastIndexOf(File.separator) + 1);									
					InputStream input = url.openConnection().getInputStream();					
					FileOutputStream output = new FileOutputStream(tempPath.toString() + File.separator + fileName);
					
					byte[] buffer = new byte[256];
					while(input.read(buffer) != -1)
					{
						output.write(buffer);
					}
					
					input.close();
					output.flush();
					output.close();								
				}
			}
			catch(Exception e)
			{
			}
		}
		
		return tempPath;
	}
}
