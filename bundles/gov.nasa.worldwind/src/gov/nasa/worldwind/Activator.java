/*******************************************************************************
 * Copyright (c) 2018 Agence spatiale canadienne / Canadian Space Agency.
 * All rights reserved. This program and the accompanying materials
 * are made available under the terms of the Eclipse Public License v1.0
 * which accompanies this distribution, and is available at
 * http://www.eclipse.org/legal/epl-v10.html
 *
 * Contributors:
 * 	 	 Pierre Allard - initial API and implementation 
 *       Regent L'Archeveque
 * SPDX-License-Identifier: EPL-1.0
 *******************************************************************************/
package gov.nasa.worldwind;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.net.URL;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.util.Enumeration;
import java.util.jar.JarEntry;
import java.util.jar.JarFile;

import org.eclipse.core.runtime.Platform;
import org.eclipse.osgi.framework.log.FrameworkLog;
import org.eclipse.osgi.framework.log.FrameworkLogEntry;
import org.osgi.framework.Bundle;
import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.framework.FrameworkUtil;
import org.osgi.framework.ServiceReference;

/**
 * The activator class controls the plug-in life cycle
 */
public class Activator implements BundleActivator {

	private static BundleContext context;

	public static final String ID = "gov.nasa.worldwind";

	public static BundleContext getContext() {
		return context;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see org.osgi.framework.BundleActivator#start(org.osgi.framework.
	 * BundleContext)
	 */
	public void start(BundleContext bundleContext) throws Exception {
		Activator.context = bundleContext;
		extractNativeLibraries();
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see
	 * org.osgi.framework.BundleActivator#stop(org.osgi.framework.BundleContext)
	 */
	public void stop(BundleContext bundleContext) throws Exception {
		Activator.context = null;		
	}

	/**
	 * Extract World Wind Native Libraries and makes a copy into the working
	 * directory.
	 */
	private void extractNativeLibraries() {
		log(ID, "Extract native libraries for " + Platform.getOS() + "." + Platform.getOSArch() + " started.",
				FrameworkLogEntry.INFO, null);

		/**
		 * Check OS.
		 */
		try {
			if (Platform.getOS().equals(Platform.OS_WIN32) && Platform.getOSArch().equals(Platform.ARCH_X86)) {
				// win32, x86
				extractNativeLibraries("gluegen-rt-natives-windows-i586.jar", ".dll");
				extractNativeLibraries("jogl-all-natives-windows-i586.jar", ".dll");

			} else if (Platform.getOS().equals(Platform.OS_WIN32)
					&& Platform.getOSArch().equals(Platform.ARCH_X86_64)) {
				// win32, x86_64
				extractNativeLibraries("gluegen-rt-natives-windows-amd64.jar", ".dll");
				extractNativeLibraries("jogl-all-natives-windows-amd64.jar", ".dll");
			} else if (Platform.getOS().equals(Platform.OS_LINUX) && Platform.getOSArch().equals(Platform.ARCH_X86)) {
				// linux, x86
				extractNativeLibraries("gluegen-rt-natives-linux-i586.jar", ".so");
				extractNativeLibraries("jogl-all-natives-linux-i586.jar", ".so");
			} else if (Platform.getOS().equals(Platform.OS_LINUX)
					&& Platform.getOSArch().equals(Platform.ARCH_X86_64)) {
				// linux, x86_64
				extractNativeLibraries("gluegen-rt-natives-linux-amd64.jar", ".so");
				extractNativeLibraries("jogl-all-natives-linux-amd64.jar", ".so");
			} else if (Platform.getOS().equals(Platform.OS_MACOSX)) {
				// macosx
				extractNativeLibraries("gluegen-rt-natives-macosx-universal.jar", ".jnilib");
				extractNativeLibraries("jogl-all-natives-macosx-universal.jar", ".jnilib");
			} else {
				throw new Exception("");
			}
			log(ID, "Extract native libraries for " + Platform.getOS() + "." + Platform.getOSArch() + " completed.",
					FrameworkLogEntry.INFO, null);

		} catch (Exception e) {
			log(ID, "Failed to extract native libraries for " + Platform.getOS() + "." + Platform.getOSArch() + " completed.",
					FrameworkLogEntry.ERROR, null);
		}

	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public void log(String bundleId, String message, int severity, Throwable t) {
		String fullMessage = message;
		FrameworkLogEntry logEntry = new FrameworkLogEntry(bundleId, severity, 0, fullMessage, 0, t, null);
		getLog().log(logEntry);
	}

	/**
	 * <!-- begin-user-doc --> <!-- end-user-doc -->
	 * 
	 * @generated_NOT
	 */
	public FrameworkLog getLog() {
		if (Activator.getContext() != null) {
			ServiceReference<?> logService = Activator.getContext()
					.getServiceReference("org.eclipse.osgi.framework.log.FrameworkLog");
			FrameworkLog log = (FrameworkLog) Activator.getContext().getService(logService);
			return log;
		} else {
			return null;
		}
	}

	/**
	 * Extract the native libraries and makes a copy into the working directory.
	 * 
	 * @param jarFilename
	 *            Name of the jar.
	 * @param libraryExtension
	 *            Extension of the native libraries.
	 * @throws Exception
	 *             Reference to the exception.
	 */
	private void extractNativeLibraries(String jarFilename, String libraryExtension) throws Exception {
		Bundle bundle = FrameworkUtil.getBundle(Activator.class);
		URL url = bundle.getEntry("jars/" + jarFilename);
		try {
			ReadableByteChannel rbc = Channels.newChannel(url.openStream());
			FileOutputStream fos = new FileOutputStream(jarFilename);
			fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
			fos.close();
		} catch (Exception e) {
			throw new Exception("Unable to open " + url, e);
		}

		try {
			File file = new File(jarFilename);
			JarFile jarFile = new JarFile(file);
			Enumeration<JarEntry> jarEntries = jarFile.entries();
			while (jarEntries.hasMoreElements()) {
				JarEntry jarEntry = jarEntries.nextElement();
				if (jarEntry.getName().indexOf(libraryExtension) != -1) {	
					ReadableByteChannel rbc = Channels.newChannel(jarFile.getInputStream(jarEntry));
					File nativeFile = new File(jarEntry.getName());
					nativeFile.deleteOnExit();
					FileOutputStream fos = new FileOutputStream(nativeFile);
					fos.getChannel().transferFrom(rbc, 0, Long.MAX_VALUE);
					fos.close();
				}
			}
			jarFile.close();
			file.delete();

		} catch (IOException e) {
			throw new Exception("Unable to extract native libaries (" + libraryExtension + ") from " + url, e);
		}
	}

}